import { readable, writable } from 'svelte/store';
import { get } from 'svelte/store';

const lsKey_hiScore = "hiscore"
let lsHiScore = localStorage.getItem(lsKey_hiScore);
lsHiScore = lsHiScore ? lsHiScore : 0;

export const ST_gamestart = writable(0);
export const ST_gameend = writable(0);

// ゲーム時間の秒数設定 (単位：ms)
export const ST_progressbar = 3000;

export const ST_score = writable(0);
export const ST_hiScore = readable(null, function start(set) {
    //readableで初期値をセットできる
    set(lsHiScore);

    //この中だけで値の更新ができる
    //スコアの変更を検知して条件によって更新する
    ST_score.subscribe(value => {
        // console.log('ST_score.subscribed  hi-score: ' + get(ST_score));
        if (lsHiScore < value) {
            set(value);
            localStorage.setItem(lsKey_hiScore, value);
        }
    });
});

export const ST_time = readable(null, function start(set) {
    set(ST_progressbar);
    ST_gamestart.subscribe(value => {
        let interval;
        if (value) {
            interval = setInterval(() => {
                set(get(ST_time) - 1);
                if (get(ST_time) <= 0) {
                    clearInterval(interval);
                    ST_gamestart.set(false);
                    ST_gameend.set(true);
                    set(ST_progressbar)
                    console.log('timer_stop!!')
                }
            }, 10);
        } else {
            clearInterval(interval);
        }
    });
});
